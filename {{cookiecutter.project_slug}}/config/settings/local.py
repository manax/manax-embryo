"""
Local settings

- 
"""

from .base import *  # noqa

ENV_FILE = str(ENV_DIR + '/local')
env.read_env(ENV_FILE)


# Your common stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

