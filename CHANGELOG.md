# Change Log
All enhancements and patches to Cookiecutter Django will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [YYYY-MM-DD]
### Changed
### Fixed
### Removed

## [2017-02-07]
### Added
- django-extensions
